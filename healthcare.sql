-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2019 at 10:17 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `healthcare`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `Name` varchar(20) NOT NULL,
  `Speciality` varchar(20) NOT NULL,
  `Contact` bigint(10) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `location` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`Name`, `Speciality`, `Contact`, `Email`, `Password`, `location`) VALUES
('', ' ', 0, '', ' ', ''),
('', ' ', 0, '', ' ', ''),
('mabibdi', ' kajnsad', 654648545, 'asnd@lajssdn.com', ' asdasd', 'oasndansd'),
('abc', ' Dermatologist', 123456789, 'abc@gmail.com', ' 123', 'MUMBAI'),
('', ' ', 0, '', ' ', '');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_details`
--

CREATE TABLE `doctor_details` (
  `dr_id` int(11) NOT NULL,
  `dr_name` varchar(50) NOT NULL,
  `dr_degree` varchar(50) NOT NULL,
  `dr_specialization` varchar(50) NOT NULL,
  `dr_location` varchar(50) NOT NULL,
  `dr_timing` time NOT NULL,
  `disease` varchar(15) NOT NULL,
  `dr_contact` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_details`
--

INSERT INTO `doctor_details` (`dr_id`, `dr_name`, `dr_degree`, `dr_specialization`, `dr_location`, `dr_timing`, `disease`, `dr_contact`) VALUES
(1, 'Dr. Ravishankar Reddy C. R.', 'MBBS, MD - General Medicine, DNB - Neurology', 'General Physician', 'Mumbai', '19:00:00', 'Malaria', 9875645678),
(3, 'Dr. Parthasarathi Dutta Roy', 'B.Sc, MBBS, DDVL, MD - Dermatology', 'Dermatologist, Trichologist, Cosmetologist', 'Mumbai', '10:30:00', 'Acne,Allergy', 4517826541),
(4, 'Dr. Parthasarathi Dutta Roy', 'B.Sc, MBBS, DDVL, MD - Dermatology', 'Dermatologist, Trichologist, Cosmetologist', 'Mumbai', '10:30:00', 'Acne,Allergy', 5874125639),
(5, 'Dr. Kukreja A. Kalani', 'MD - Homeopathy, DHMS', 'Dermatologist, Homoeopath', 'Mumbai', '12:30:00', 'Allergy', 9874578458),
(6, 'Dr. Kukreja A. Kalani', 'MD - Homeopathy, DHMS', 'Dermatologist, Homoeopath', 'Mumbai', '12:30:00', 'Allergy', 8745127845),
(7, 'Dr. Indu Bubna', 'Diploma in Tuberculosis and Chest Diseases (DTCD),', 'Pulmonologist, Tuberculous and chest Diseases ', 'Mumbai', '09:30:00', 'Asthma', 9874512556);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctor_details`
--
ALTER TABLE `doctor_details`
  ADD PRIMARY KEY (`dr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctor_details`
--
ALTER TABLE `doctor_details`
  MODIFY `dr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
