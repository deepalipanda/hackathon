<html lang="en">
<head>
    <!-- <link rel="stylesheet" href="./semantic.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.0/semantic.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.0/semantic.min.js">
    <link rel="stylesheet" crossorigin="anonymous" href="/94A03259-0C7C-DF45-91FB-9AE4B7650E63/abn/main.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudfare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudfare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body{
            padding-top: 12.5%;
            overflow: hidden;}
        ::placeholder{
            color:teal;
        }
        :-ms-input-placeholder{
            color:teal;
        }
    </style>
</head>
<body>
        <div class="ui centered raised grid">
            <div class="ui content">
                <div class="ui compact segment">
                    <div class="ui teal bold left floated header">Login</div>

                    <form class="ui form">
                        <div class="ui required field">
                           <div class="ui teal placeholder">
                            <input type="email" placeholder="Email id" name="user_contact_email" id="user_contact_email"/>
                        </div>
                    </div>
                        <div class="ui required left floated field">
                            <input type="password" placeholder="Password" style="-ms-input-placeholder:teal" name="user_pass_auth" id="user_pass_auth" />
                        </div>

                        <a href="doc_land.php"><button  class="ui fluid teal button" name="user_auth_btn" id="user_auth_btn">Login</button></a>
                    </form>
                </div>
            </div>
    </body>
</html>
