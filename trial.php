<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.0/semantic.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.0/semantic.min.js">
  <script type="text/javascript">
  $('.menu .item')
  .tab()
  ;

  </script>
</head>
<body>

  <div class="ui top attached tabular menu">
    <a class="item" data-tab="first">First</a>
    <a class="item active" data-tab="second">Second</a>
    <a class="item" data-tab="third">Third</a>
  </div>
  <div class="ui bottom attached tab segment" data-tab="first">
    First
  </div>
  <div class="ui bottom attached tab segment active" data-tab="second">
    Second
  </div>
  <div class="ui bottom attached tab segment" data-tab="third">
    Third
  </div>
</body>
</html>
