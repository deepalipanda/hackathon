<?php
include_once("api/core.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Health.io</title>


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <link href="css/shop-homepage.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <link href="fonts.googleapis.com/css?family=Hind"; rel="stylesheet">


  <style>
  .customdiv{
    margin-top: 50px;
    margin-bottom: 50px;
  }
  .call,.assist,.doctorlist{
    opacity: 0.7;

  }
  .call:hover,.assist:hover,.doctorlist:hover{
    opacity: 1;
  }
  #id02{
    display: none;
  }
  .middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.assistant:hover .assist {
  opacity: 1;
}

.assistant:hover .middle {
  opacity: 1;
}
.doctorlist:hover .assist {
  opacity: 1;
}

.doctorlist:hover .middle {
  opacity: 1;
}
.call:hover .assist {
  opacity: 1;
}

.call:hover .middle {
  opacity: 1;
}
a{
  text-decoration: none;
}
  </style>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>


<script type="text/javascript">
if(document.getElementById('english').checked){
  alert("Choosen language is English");
  console.log('English');
}
else{
  alert("Choosen language is Hindi");
  console.log('Hindi');
}

$(document).ready(function(){
  $("#id02").hide();
$("#are").click(function(){
  $("#are").hide();
  $("#id02").show();

});
});


</script>
</head>
<body onload="document.getElementById('id01').style.display='block'">

<div id="id01" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container ">

        <h2>Select Language</h2>
      </header>
      <div class="w3-container">
        <form method="post" action="#">

    <div class="radio">
      <label><input type="radio" id="english" value="english" name="optradio">English</label>
    </div>
    <div class="radio">
      <label><input type="radio" id="hindi" value="hindi" name="optradio">हिंदी</label>
    </div>

  <div class="radio">
    <label><input type="radio" id="marathi" value="marathi" name="optradio">मराठी</label>
  </div>
  </form>
      </div>
      <footer class="w3-container ">
        <button type="submit" class="btn btn-outline-success" onclick="document.getElementById('id01').style.display='none'" name="submit">OK</button>
      </footer>
    </div>
  </div>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Health.io </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">

          <li class="nav-item">
<button type="button" id="are" class="btn btn-info btn-lg" data-toggle="modal">Are you a doctor?</button>
<!-- <button type="button" id="are" class="btn btn-info btn-lg" data-toggle="modal">क्या आप एक डॉक्टर हैं?
</button> -->

<div id="id02">
  <a href="user_auth.php"><button type="button" class="btn btn-info" >Login</button></a>
  <a href="user_registration.php"><button type="button" class="btn btn-info" >Register</button></a>
  <!-- <a href="user_auth.php"><button type="button" class="btn btn-info" >लॉग इन करें
</button></a>
  <a href="user_registration.php"><button type="button" class="btn btn-info" >रजिस्टर
</button></a> -->
</div>

          </li>

        </ul>
      </div>
    </div>
  </nav>



  <div class="container">
    <div class="row customdiv">
          <div class="col-lg-4 col-md-4 mb-4 assistant">
            <div class="card h-130">
              <a onclick="document.getElementById('assistant').style.display='block'"><img class="card-img-top assist" src="img/assistant.jpg" height="350px" width="500px" alt=""></a>
              <div class="middle">
    <div class="text"><a onclick="document.getElementById('assistant').style.display='block'"> <h3>ASSISTANT</h3></a></div>
    <!-- <div class="text"><a onclick="document.getElementById('assistant').style.display='block'"> <h3>सहायक
</h3></a></div> -->

  </div>
            </div>
          </div>
          <div id="assistant" class="w3-modal">
              <div class="w3-modal-content w3-animate-top w3-card-4">
                <header class="w3-container ">

                  <h2>ASSISTANT</h2>
                  <!-- <h2>सहायक</h2> -->

                </header>

                <footer class="w3-container ">
                  <button type="submit" class="btn btn-outline-success" onclick="document.getElementById('assistant').style.display='none'">Close</button>
                  <!-- <button type="submit" class="btn btn-outline-success" onclick="document.getElementById('assistant').style.display='none'">बंद करे
</button> -->

                </footer>
              </div>
            </div>


          <div class="col-lg-4 col-md-4 mb-4 doctorlist">
            <div class="card h-130">
              <a onclick="document.getElementById('doctorlist').style.display='block'"><img class="card-img-top call" src="img/doctor.jpeg" height="350px" width="500px" alt=""></a>
              <div class="middle">
              <div class="text">  <a onclick="document.getElementById('doctorlist').style.display='block'"><h3>DOCTORS LIST</h3></a></div>
              <!-- <div class="text">  <a onclick="document.getElementById('doctorlist').style.display='block'"><h3>डॉक्टरों की सूची
</h3></a></div> -->

              </div>
              <!-- <div class="card-body">
                <h4 class="card-title">
                  Call a Doctor
                </h4>
                <h4 class="card-title">
                  Chat with a Doctor
                </h4>
              </div> -->
            </div>
          </div>
          <div id="doctorlist" class="w3-modal">
              <div class="w3-modal-content w3-animate-top w3-card-4">
                <header class="w3-container ">

                  <h2>List of doctors available</h2>
                  <!-- <h2>उपलब्ध डॉक्टरों की सूची
</h2> -->

                  <?php
                  $conn = connection();
                  $sql = "SELECT * FROM doctor_details";
                  $result = $conn->query($sql);
                  if($result->num_rows>0){
                    echo "DoctorName &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;              Degree&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact";
                    echo"<br>";
                    while ($row=$result->fetch_assoc()) {
                      // code...
                      echo $row['dr_name']." ".$row['dr_degree']. " ".$row['dr_contact'];
                      echo"<hr>";
                    }
                  }
?>
                </header>

                <footer class="w3-container ">
                  <button type="submit" class="btn btn-outline-success" onclick="document.getElementById('doctorlist').style.display='none'">Close</button>
                </footer>
              </div>
            </div>

          <div class="col-lg-4 col-md-4 mb-4 call">
            <div class="card h-130">
              <a onclick="document.getElementById('connectdoc').style.display='block'"><img class="card-img-top call" src="img/call.jpg" height="350px" width="500px" alt=""></a>
              <div class="middle">
              <div class="text"><a onclick="document.getElementById('connectdoc').style.display='block'"><h3>CONNECT TO A DOCTOR</h3></a></div>
              <!-- <div class="text"><a onclick="document.getElementById('connectdoc').style.display='block'"><h3>एक डॉक्टर से कनेक्ट करें
</h3></a></div> -->

              </div>
            </div>
          </div>
          <div id="connectdoc" class="w3-modal">
              <div class="w3-modal-content w3-animate-top w3-card-4">
                <header class="w3-container ">

                  <h2>Connect to a doctor</h2>
                  <!-- <h2>एक डॉक्टर से कनेक्ट करें
</h2> -->


                </header>


                <footer class="w3-container ">
                  <button type="submit" class="btn btn-outline-success" onclick="document.getElementById('connectdoc').style.display='none'">Close</button>
                </footer>
              </div>
            </div>
        </div>
      </div>


  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Health.io 2019</p>
    </div>
  </footer>



</body>

</html>
